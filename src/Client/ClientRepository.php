<?php
declare(strict_types=1);

namespace Lookyman\NetteOAuth2Server\Storage\Doctrine\Client;

use Kdyby\Doctrine\InvalidStateException;
use Kdyby\Doctrine\QueryException;
use Kdyby\Doctrine\Registry;
use League\OAuth2\Server\Repositories\ClientRepositoryInterface;
use Nette\Utils\Random;

class ClientRepository implements ClientRepositoryInterface
{
    /**
     * @var Registry
     */
    private $registry;

    /**
     * @var callable
     */
    private $secretValidator;

    /**
     * @param Registry $registry
     * @param callable|null $secretValidator
     */
    public function __construct(Registry $registry, callable $secretValidator = null)
    {
        $this->registry = $registry;
        $this->secretValidator = $secretValidator ?: function ($expected, $actual) { return hash_equals($expected, $actual); };
    }

    /**
     * @param string $clientIdentifier
     * @param string $grantType
     * @param string|null $clientSecret
     * @param bool $mustValidateSecret
     * @return ClientEntity|null
     * @throws InvalidStateException
     * @throws QueryException
     */
    public function getClientEntity($clientIdentifier, $grantType, $clientSecret = null, $mustValidateSecret = true)
    {
        /** @var ClientEntity|null $clientEntity */
        $clientEntity = $this->registry->getManager()->getRepository(ClientEntity::class)->fetchOne($this->createQuery()->byIdentifier($clientIdentifier));
        return $clientEntity
        && $mustValidateSecret
        && $clientEntity->getSecret() !== null
        && !call_user_func($this->secretValidator, $clientEntity->getSecret(), $clientSecret)
            ? null
            : $clientEntity;
    }

    /**
     * @param        $name
     * @param string $description
     * @param null   $clientEntity
     *
     * @throws \Exception
     */
    public function createClientEntity($name, $description = '', $clientEntity = null)
    {
        if (is_null($clientEntity)) {
            $clientEntity = new ClientEntity();
        }

        $clientEntity->setRedirectUri(' ');
        $clientEntity->setName($name);
        $clientEntity->setDescription($description);
        $clientEntity->setIdentifier(Random::generate(32));
        $clientEntity->setSecret(Random::generate(32));

        $manager = $this->registry->getManager();
        $manager->persist($clientEntity);
        $manager->flush();
    }

    /**
     * Returns ClientEntity just for displaying in list. This is not a method used by server client validation.
     *
     * @param $id
     *
     * @return null|ClientEntity
     */
    public function getClientForList($id)
    {
        return $this->registry->getManager()->getRepository(ClientEntity::class)->findOneBy(['id' => $id]);
    }

    /**
     * @param int|null $id
     *
     * @return bool
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Exception
     */
    public function deleteClientEntity(int $id = null)
    {
        if (is_null($id)) return false;

        $manager = $this->registry->getManager();

        $clientEntity = $manager->createQueryBuilder()
                                ->select('c')
                                ->from(ClientEntity::class, 'c')
                                ->where('c.id=:id')->setParameter('id', $id)
                                ->getQuery()->getOneOrNullResult();

        if (is_null($clientEntity)) return false;

        $manager->remove($clientEntity);
        $manager->flush();

        return true;
    }

    /**
     * @param int|null $id
     *
     * @return null|ClientEntity
     * @throws \Exception
     */
    public function regenerateClientCredentials(int $id = null)
    {
        if (is_null($id)) return null;

        $manager = $this->registry->getManager();

        /** @var ClientEntity $clientEntity */
        $clientEntity = $manager->getRepository(ClientEntity::class)->findOneBy(['id' => $id]);

        if (is_null($clientEntity)) return null;

        $identifier = Random::generate(32);
        $secret = Random::generate(32);

        $clientEntity->setIdentifier($identifier);
        $clientEntity->setSecret($secret);

        $manager->flush();
        return $clientEntity;
    }

    /**
     * @return array
     */
    public function getClientEntities()
    {
        return $this->registry->getRepository(ClientEntity::class)->findAll();
    }

    /**
     * @return ClientQuery
     */
    protected function createQuery(): ClientQuery
    {
        return new ClientQuery();
    }
}
